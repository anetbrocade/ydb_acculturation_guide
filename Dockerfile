FROM yottadb/yottadb:r1.29
LABEL maintainer="Bart Moelans <bart.moelans@uantwerpen.be>"

##################################
# Define global environment vars #
##################################
ENV ydb_dist=/opt/yottadb/current \
    gtmdir=/home/yottadbuser/.yottadb \
    ydb_dir=/home/yottadbuser/.yottadb \
    ydb_chset=utf-8


#############################
# 1) Configure MUMPS        #
# 2) Add user               #
# 3) Update apt-get         #
# 4) Install extra software #
#############################

RUN useradd -ms /bin/bash -g root -G sudo yottadbuser \
    && echo "yottadbuser:YottaDB Rocks!" | chpasswd \
    && apt-get -y update \
    && apt-get -y install sudo tree gcc

USER yottadbuser
WORKDIR /home/yottadbuser

# by default yottadb entrypoint is ydb itself
ENTRYPOINT []
