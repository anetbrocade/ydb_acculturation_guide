# Acculturation Guide using Docker #

## Setup Docker

Make sure you have Docker Desktop installed (https://www.docker.com/products/docker-desktop)


## Quick Start

### Install YottaDB

 * Get the docker repo: `git clone https://bmoelans@bitbucket.org/anetbrocade/ydb_acculturation_guide.git`
 * Go to the directory where `Dockerfile` is located
 * Build the container with `docker-compose build`

### Run YottaDB

While still in the folder with the `Dockerfile`, run `docker-compose run --rm yottadb bash`. This will enter the YottaDB container in a bash environment.

### Using YottaDB

#### Access from C
C file is included in repository


## Journaling
to kill container to simulate crash `docker kill $(docker ps -q --filter ancestor=ydb_acculturation_guide_yottadb)`